import { IsNotEmpty, Length, Matches } from 'class-validator';

export class CreateUserDto {
  @Length(4, 16)
  @IsNotEmpty()
  login: string;

  @Length(4, 16)
  @IsNotEmpty()
  name: string;

  @Matches(/^(?=.*\d)(?=.*[!@#$%^&*])(?=.*[a-z])(?=.*[A-Z]).{8,}$/)
  @Length(8, 16)
  @IsNotEmpty()
  password: string;
}
