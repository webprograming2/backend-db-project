import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class User {
  @PrimaryGeneratedColumn({ name: 'user_id' })
  id: number;

  @Column({ name: 'user_login', unique: true })
  login: string;

  @Column({ name: 'user_name' })
  name: string;

  @Column({ name: 'user_password' })
  password: string;
}
